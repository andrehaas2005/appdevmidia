package app.devmedia.com.br.appdevmidia.util;

/**
 * Created by andre.haas on 05/01/2017.
 */

public class Constantes {
    public static final String URL_WS_BASE = "http://perola.andre.haas.nom.br/servico/api/";//pessoa/GetPessoabyRamo/?id=1&limit=1";
    //public static final String URL_WS_BASE = "https://13ed8388.ngrok.io/api/";//pessoa/GetPessoabyRamo/?id=1&limit=1";

    public static final String URL_IMG_BASE = "http://perola.andre.haas.nom.br/adm/uploads/fotos/pessoa/app/";
}

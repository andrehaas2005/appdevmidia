package app.devmedia.com.br.appdevmidia.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import app.devmedia.com.br.appdevmidia.R;
import app.devmedia.com.br.appdevmidia.adapter.IncentivoRecyclerAdapter;
import app.devmedia.com.br.appdevmidia.adapter.PessoaRecyclerAdapter;
import app.devmedia.com.br.appdevmidia.model.Incentivo;
import app.devmedia.com.br.appdevmidia.model.Pessoa;
import app.devmedia.com.br.appdevmidia.util.Constantes;


import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.devmedia.com.br.appdevmidia.util.util;
import cz.msebera.android.httpclient.Header;

import static android.R.attr.type;


/**
 * Created by andre.haas on 05/01/2017.
 */

public class FragmentIncentivo extends Fragment {

    private RecyclerView recvince;
    final Gson gson = new Gson();
    public List<Incentivo> incentivos;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View viewRoot = inflater.inflate(R.layout.fragment_incentivo, container, false);
        incentivos = new ArrayList<Incentivo>();
        final RelativeLayout lyLoadingI = (RelativeLayout) viewRoot.findViewById(R.id.lyLoadingI);
        lyLoadingI.setVisibility(View.VISIBLE);
        recvince = (RecyclerView) viewRoot.findViewById(R.id.recvince);
        recvince.setLayoutManager(new LinearLayoutManager(getContext()));

        new AsyncHttpClient().get(Constantes.URL_WS_BASE + "incentivo/", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                if (response != null) {
                    Type type = new TypeToken<List<Incentivo>>() {
                    }.getType();
                    incentivos = gson.fromJson(response.toString(), type);
                }
                IncentivoRecyclerAdapter incentivoRecyclerAdapter = new IncentivoRecyclerAdapter(incentivos);
                recvince.setAdapter(incentivoRecyclerAdapter);
                lyLoadingI.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Toast.makeText(getContext(), "Falha: => " + responseString, Toast.LENGTH_SHORT).show();
            }
        });


        return viewRoot;


    }

}

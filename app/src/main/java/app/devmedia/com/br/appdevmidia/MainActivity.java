package app.devmedia.com.br.appdevmidia;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import app.devmedia.com.br.appdevmidia.Fragment.FragmentIncentivo;
import app.devmedia.com.br.appdevmidia.Fragment.FragmentPessoa;
import app.devmedia.com.br.appdevmidia.adapter.ViewPagerAdapter;


public class MainActivity extends AppCompatActivity {


    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private Drawer drawer;

//    private Drawable drawable;
//
//    private static final long ID_ND_FOOTER = 500;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //new DrawerBuilder().withActivity(this).build();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        viewPager = (ViewPager) findViewById(R.id.viewPager);
        configurarViewPage(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


        final PrimaryDrawerItem item1 = new PrimaryDrawerItem().withName("Lista de Lobos");
        final SecondaryDrawerItem item2 = new SecondaryDrawerItem().withName("Incentivo (Patinhas)").withBadge("Novo")
                .withBadgeStyle(
                        new BadgeStyle().withTextColor(Color.WHITE)
                                .withColorRes(R.color.md_orange_700));

        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header)
                .addProfiles(
                        new ProfileDrawerItem()
                                .withName("André Haas")
                                .withEmail("andrehaas2005@gmail.com")
                                .withIcon(getResources()
                                        .getDrawable(R.drawable.profile))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean current) {
                        return false;
                    }
                })
                .build();




        drawer = new DrawerBuilder().
                withAccountHeader(headerResult)
                .withActivity(this)
                .withToolbar(toolbar)
                .addDrawerItems(item1,
                        new DividerDrawerItem(),
                        item2

                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Toast.makeText(MainActivity.this, "pos: " + position, Toast.LENGTH_SHORT).show();
                        viewPager.setCurrentItem(position, true);
                        if (position == 2) {
                            item1.withName("Lobos")
                                    .withBadge("5")
                                    .withBadgeStyle(
                                            new BadgeStyle().withTextColor(Color.WHITE)
                                                    .withColorRes(R.color.md_blue_300));
                            drawer.updateItem(item1);
                        }
                        drawer.closeDrawer();
                        return true;
                    }
                }).addDrawerItems(new DividerDrawerItem()).addStickyDrawerItems(new PrimaryDrawerItem().withName("Sair"))
                .build();

    }


    private void configurarViewPage(ViewPager viewPager) {

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new FragmentPessoa(), "Lista Lobos");
        viewPagerAdapter.addFragment(new FragmentIncentivo(), "Incentivo");
        // viewPagerAdapter.addFragment(new FragmentPerfil(), "Perfil");
        viewPager.setAdapter(viewPagerAdapter);

    }


}

package app.devmedia.com.br.appdevmidia.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.lang.reflect.Type;

import app.devmedia.com.br.appdevmidia.DetalhesActivity;
import app.devmedia.com.br.appdevmidia.R;
import app.devmedia.com.br.appdevmidia.model.Pessoa;
import app.devmedia.com.br.appdevmidia.util.Constantes;
import cz.msebera.android.httpclient.Header;

/**
 * Created by andre.haas on 05/01/2017.
 */

public class FragmentPerfil extends Fragment {

    private TextView txNomePerfil;
    private TextView tvEndereco;
    private TextView tvTelefone;
    private TextView tvResponsavel;
    private TextView tvCidade;

    private Pessoa pessoa;
    Gson gson = new Gson();



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_perfil, container, false);

        final RelativeLayout lytLoading = (RelativeLayout) view.findViewById(R.id.lyLoading);

        txNomePerfil = (TextView) view.findViewById(R.id.txNomePerfil);
        tvEndereco = (TextView) view.findViewById(R.id.tvEndereco);
        tvTelefone = (TextView) view.findViewById(R.id.tvTelefone);
        tvResponsavel = (TextView) view.findViewById(R.id.tvResponsavel);
        tvCidade = (TextView) view.findViewById(R.id.tvCidade);


        Bundle data = getArguments();
        String valor = data.getString("idPessoa");

        RequestParams param = new RequestParams();
        param.put("id", valor);

        lytLoading.setVisibility(View.VISIBLE);

        new AsyncHttpClient().get(Constantes.URL_WS_BASE + "pessoa/GetPessoaById/", param, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if (response != null) {
                    Type type = new TypeToken<Pessoa>() {
                    }.getType();
                    pessoa = gson.fromJson(response.toString(), type);

                    txNomePerfil.setText(pessoa.getNome());
                    tvResponsavel.setText(pessoa.getNomeResponsavel());
                    tvEndereco.setText(pessoa.getEndereco());
                    tvCidade.setText(pessoa.getCidade());
                    tvTelefone.setText(pessoa.getTelefone());
                    lytLoading.setVisibility(View.GONE);
                }

            }
        });



        return view;
    }


}

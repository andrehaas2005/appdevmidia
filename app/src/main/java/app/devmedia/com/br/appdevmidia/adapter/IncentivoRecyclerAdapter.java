package app.devmedia.com.br.appdevmidia.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import app.devmedia.com.br.appdevmidia.R;
import app.devmedia.com.br.appdevmidia.model.Incentivo;
import app.devmedia.com.br.appdevmidia.model.Pessoa;
import app.devmedia.com.br.appdevmidia.util.Constantes;

/**
 * Created by andre.haas on 27/01/2017.
 */

public class IncentivoRecyclerAdapter extends RecyclerView.Adapter<IncentivoRecyclerAdapter.ViewHolder> {

    private final List<Incentivo> incentivos;

    public IncentivoRecyclerAdapter(List<Incentivo> incentivos) {
        this.incentivos = incentivos;
    }

    @Override
    public IncentivoRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.linha_incentivo, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(IncentivoRecyclerAdapter.ViewHolder holder, int position) {

        Incentivo incentivo = incentivos.get(position);
        holder.txtNome.setText(incentivo.getPessoa().getNome());

        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        Date date = null;
        try {
            date = inputFormat.parse(incentivo.getDataIncentivo());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        String dateString = outputFormat.format(date);

        holder.txtData.setText(dateString);

         Picasso.with(holder.imagePessoInc.getContext()).load(Constantes.URL_IMG_BASE + incentivo.getPessoa().getFoto()).into(holder.imagePessoInc);

    }

    @Override
    public int getItemCount() {
        return incentivos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imagePessoInc;
        TextView txtNome;
        TextView txtData;
        TextView txtPata;
        Button btnConfirma;


        public ViewHolder(final View itemView) {
            super(itemView);
            imagePessoInc = (ImageView) itemView.findViewById(R.id.imgPessoaInc);
            txtNome = (TextView) itemView.findViewById(R.id.txtNome);
            txtData = (TextView) itemView.findViewById(R.id.txtDataInc);
            txtPata = (TextView) itemView.findViewById(R.id.txtPata);
            btnConfirma = (Button) itemView.findViewById(R.id.btnConfirmaInc);
            btnConfirma.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(itemView.getContext(), "Card Icentivo clicado", Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
}

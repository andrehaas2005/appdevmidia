package app.devmedia.com.br.appdevmidia;

import android.app.Application;

import android.os.SystemClock;

import java.util.concurrent.TimeUnit;

/**
 * Created by andre.haas on 05/01/2017.
 */

public class DevMidiaApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SystemClock.sleep(TimeUnit.SECONDS.toMillis(3));

    }
}

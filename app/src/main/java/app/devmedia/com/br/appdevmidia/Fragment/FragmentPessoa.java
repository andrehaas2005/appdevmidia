package app.devmedia.com.br.appdevmidia.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.devmedia.com.br.appdevmidia.DetalhesActivity;
import app.devmedia.com.br.appdevmidia.R;
import app.devmedia.com.br.appdevmidia.adapter.PessoaRecyclerAdapter;
import app.devmedia.com.br.appdevmidia.model.Pessoa;
import app.devmedia.com.br.appdevmidia.model.PessoaViewModel;
import app.devmedia.com.br.appdevmidia.util.Constantes;
import app.devmedia.com.br.appdevmidia.util.RecyclerItemClickListener;
import cz.msebera.android.httpclient.Header;


/**
 * Created by andre.haas on 05/01/2017.
 */

public class FragmentPessoa extends Fragment {

    private RecyclerView rv;
    private List<PessoaViewModel> pessoas;
    final Gson gson = new Gson();
    private Pessoa pessoaClicada;
    //  final String strGet = "pessoa/GetPessoabyRamo/";
    final String strGet = "pessoa/GetByRamo/";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        pessoas = new ArrayList<PessoaViewModel>();
        final View viewRoot = inflater.inflate(R.layout.fragment_pessoa, container, false);

        final RelativeLayout lytLoading = (RelativeLayout) viewRoot.findViewById(R.id.lyLoading);


        lytLoading.setVisibility(View.VISIBLE);

        rv = (RecyclerView) viewRoot.findViewById(R.id.rv);

        rv.setLayoutManager(new LinearLayoutManager(getContext()));

        RequestParams param = new RequestParams();
        param.put("id", "1");
        //  param.put("limit", "40");


        new AsyncHttpClient().get(Constantes.URL_WS_BASE.concat(strGet), param, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                if (response != null) {
                    Type type = new TypeToken<List<PessoaViewModel>>() {
                    }.getType();

                    pessoas = gson.fromJson(response.toString(), type);
                }
                PessoaRecyclerAdapter pessoaRecyclerAdapter = new PessoaRecyclerAdapter(pessoas);
                rv.setAdapter(pessoaRecyclerAdapter);
                lytLoading.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Toast.makeText(getActivity(), "Falha: " + responseString, Toast.LENGTH_SHORT).show();
            }


        });

        rv.addOnItemTouchListener(new RecyclerItemClickListener(getContext(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(final View view, int position) {
                        Intent i = new Intent(getContext(), DetalhesActivity.class);
                        i.putExtra("idPessoa", pessoas.get(position).getIdPessoa().toString());
                        startActivity(i);


                    }
                }));




        return viewRoot;
    }


}

package app.devmedia.com.br.appdevmidia;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import java.lang.reflect.Type;

import app.devmedia.com.br.appdevmidia.Fragment.FragmentPerfil;
import app.devmedia.com.br.appdevmidia.adapter.ViewPagerAdapter;
import app.devmedia.com.br.appdevmidia.model.Pessoa;
import app.devmedia.com.br.appdevmidia.util.Constantes;
import cz.msebera.android.httpclient.Header;

public class DetalhesActivity extends AppCompatActivity {

    private FragmentPerfil fragmentPerfil;
    private ViewPager vpDetalhes;
    private TextView txNome;

    private TextView tvEndereco;
    private TextView tvTelefone;
    private TextView tvResponsavel;
    private TextView tvCidade;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes);


        fragmentPerfil = new FragmentPerfil();
        Bundle bundle = new Bundle();

        Intent i = getIntent();
        String idPessoa = i.getExtras().getString("idPessoa");
        bundle.putString("idPessoa", idPessoa);

        vpDetalhes = (ViewPager) findViewById(R.id.vpDetalhes);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        fragmentPerfil.setArguments(bundle);
        viewPagerAdapter.addFragment(fragmentPerfil, "Perfil");

        vpDetalhes.setAdapter(viewPagerAdapter);









    }


}

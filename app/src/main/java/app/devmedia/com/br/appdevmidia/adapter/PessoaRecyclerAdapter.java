package app.devmedia.com.br.appdevmidia.adapter;

import android.icu.text.DateFormat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import app.devmedia.com.br.appdevmidia.R;
import app.devmedia.com.br.appdevmidia.model.PessoaViewModel;
import app.devmedia.com.br.appdevmidia.util.Constantes;

/**
 * Created by andre.haas on 11/01/2017.
 */

public class PessoaRecyclerAdapter extends RecyclerView.Adapter<PessoaRecyclerAdapter.ViewHolder> {

    private final List<PessoaViewModel> pessoas;

    public PessoaRecyclerAdapter(List<PessoaViewModel> pessoas) {
        this.pessoas = pessoas;
    }

    @Override
    public PessoaRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.linha_pessoa, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PessoaRecyclerAdapter.ViewHolder holder, int position) {

        PessoaViewModel pessoa = pessoas.get(position);
        holder.txtTitulo.setText(pessoa.getNome());
        holder.txtDescricao.setText(String.valueOf(pessoa.getNumeroRegistro()));

        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = null;
        try {
            date = inputFormat.parse(pessoa.getDataNascimento());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        String dateString = outputFormat.format(date);


        holder.txtPreco.setText(dateString);


        Picasso.with(holder.imgProduto.getContext()).load(Constantes.URL_IMG_BASE + pessoa.getFoto()).into(holder.imgProduto);



    }

    @Override
    public int getItemCount() {
        return pessoas.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgProduto;
        TextView txtTitulo;
        TextView txtDescricao;
        TextView txtPreco;
        Button btnDetalhes;


        public ViewHolder(final View itemView) {
            super(itemView);
            imgProduto = (ImageView) itemView.findViewById(R.id.imgProduto);
            txtTitulo = (TextView) itemView.findViewById(R.id.txtTitulo);
            txtDescricao = (TextView) itemView.findViewById(R.id.txtDescricao);
            txtPreco = (TextView) itemView.findViewById(R.id.txtPreco);
            btnDetalhes = (Button) itemView.findViewById(R.id.btnDetalhes);
            btnDetalhes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });


        }
    }


}

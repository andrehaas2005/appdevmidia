package app.devmedia.com.br.appdevmidia.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by andre.haas on 27/01/2017.
 */

public class Incentivo {

    @SerializedName("idIncentito")
    private int idIncentito;

    @SerializedName("idPessoa")
    private int idPessoa;

    @SerializedName("DataIncentivo")
    private String  DataIncentivo;

    @SerializedName("DataControle")
    private String DataControle;

    @SerializedName("Ativo")
    private boolean Ativo;

    @SerializedName("idRamo")
    private int idRamo;

    @SerializedName("Pessoa")
    private Pessoa pessoa;

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }


    public int getIdIncentito() {
        return idIncentito;
    }

    public void setIdIncentito(int idIncentito) {
        this.idIncentito = idIncentito;
    }

    public int getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(int idPessoa) {
        this.idPessoa = idPessoa;
    }

    public String getDataIncentivo() {
        return DataIncentivo;
    }

    public void setDataIncentivo(String dataIncentivo) {
        DataIncentivo = dataIncentivo;
    }

    public String getDataControle() {
        return DataControle;
    }

    public void setDataControle(String dataControle) {
        DataControle = dataControle;
    }

    public boolean isAtivo() {
        return Ativo;
    }

    public void setAtivo(boolean ativo) {
        Ativo = ativo;
    }

    public int getIdRamo() {
        return idRamo;
    }

    public void setIdRamo(int idRamo) {
        this.idRamo = idRamo;
    }
}

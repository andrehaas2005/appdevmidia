package app.devmedia.com.br.appdevmidia.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by andre.haas on 05/01/2017.
 */

public class Pessoa {
    @SerializedName("idPessoa")
    private int idPessoa;
    @SerializedName("Nome")
    private String Nome;
    @SerializedName("NumeroRegistro")
    private int NumeroRegistro;
    @SerializedName("Sexo")
    private String Sexo;
    @SerializedName("NomeResponsavel")
    private String NomeResponsavel;
    @SerializedName("idEndereco")
    private int idEndereco;
    @SerializedName("DataRegistro")
    private String DataRegistro;
    @SerializedName("DataInicio")
    private String DataInicio;

    @SerializedName("DataPromessa")
    private String DataPromessa;

    @SerializedName("DataNascimento")
    private String DataNascimento;

    @SerializedName("Ativo")
    private boolean Ativo;

    @SerializedName("PagaMensalidade")
    private boolean PagaMensalidade;

    @SerializedName("FundoRegistro")
    private boolean FundoRegistro;

    @SerializedName("Foto")
    private String Foto;

    @SerializedName("idRamo")
    private int idRamo;

    @SerializedName("Email")
    private String Email;

    @SerializedName("Telefone")
    private String Telefone;

    @SerializedName("CEP")
    private String CEP;

    @SerializedName("Endereco")
    private String Endereco;

    @SerializedName("Cidade")
    private String Cidade;

    @SerializedName("Estado")
    private String Estado;

    @SerializedName("Numero")
    private int Numero;

    @SerializedName("Complemento")
    private String Complemento;

    @SerializedName("Bairro")
    private String Bairro;


    public String getCEP() {
        return CEP;
    }

    public void setCEP(String CEP) {
        this.CEP = CEP;
    }

    public int getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(int idPessoa) {
        this.idPessoa = idPessoa;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public int getNumeroRegistro() {
        return NumeroRegistro;
    }

    public void setNumeroRegistro(int numeroRegistro) {
        NumeroRegistro = numeroRegistro;
    }

    public String getSexo() {
        return Sexo;
    }

    public void setSexo(String sexo) {
        Sexo = sexo;
    }

    public String getNomeResponsavel() {
        return NomeResponsavel;
    }

    public void setNomeResponsavel(String nomeResponsavel) {
        NomeResponsavel = nomeResponsavel;
    }

    public int getIdEndereco() {
        return idEndereco;
    }

    public void setIdEndereco(int idEndereco) {
        this.idEndereco = idEndereco;
    }

    public String getDataRegistro() {
        return DataRegistro;
    }

    public void setDataRegistro(String dataRegistro) {
        DataRegistro = dataRegistro;
    }

    public String getDataInicio() {
        return DataInicio;
    }

    public void setDataInicio(String dataInicio) {
        DataInicio = dataInicio;
    }

    public String getDataPromessa() {
        return DataPromessa;
    }

    public void setDataPromessa(String dataPromessa) {
        DataPromessa = dataPromessa;
    }

    public String getDataNascimento() {
        return DataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        DataNascimento = dataNascimento;
    }

    public boolean isAtivo() {
        return Ativo;
    }

    public void setAtivo(boolean ativo) {
        Ativo = ativo;
    }

    public boolean isPagaMensalidade() {
        return PagaMensalidade;
    }

    public void setPagaMensalidade(boolean pagaMensalidade) {
        PagaMensalidade = pagaMensalidade;
    }

    public boolean isFundoRegistro() {
        return FundoRegistro;
    }

    public void setFundoRegistro(boolean fundoRegistro) {
        FundoRegistro = fundoRegistro;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }

    public int getIdRamo() {
        return idRamo;
    }

    public void setIdRamo(int idRamo) {
        this.idRamo = idRamo;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getTelefone() {
        return Telefone;
    }

    public void setTelefone(String telefone) {
        Telefone = telefone;
    }

    public String getEndereco() {
        return Endereco;
    }

    public void setEndereco(String endereco) {
        Endereco = endereco;
    }

    public String getCidade() {
        return Cidade;
    }

    public void setCidade(String cidade) {
        Cidade = cidade;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public int getNumero() {
        return Numero;
    }

    public void setNumero(int numero) {
        Numero = numero;
    }

    public String getComplemento() {
        return Complemento;
    }

    public void setComplemento(String complemento) {
        Complemento = complemento;
    }

    public String getBairro() {
        return Bairro;
    }

    public void setBairro(String bairro) {
        Bairro = bairro;
    }


}

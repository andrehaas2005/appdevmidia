package app.devmedia.com.br.appdevmidia.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by andre.haas on 23/06/2017.
 */

public class PessoaViewModel {

    @SerializedName("idPessoa")
    private Integer idPessoa;

    @SerializedName("Nome")
    private String Nome;


    @SerializedName("Foto")
    private String Foto;

    @SerializedName("NumeroRegistro")
    private Integer NumeroRegistro;

    @SerializedName("DataNascimento")
    private String DataNascimento;

    public Integer getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(Integer idPessoa) {
        this.idPessoa = idPessoa;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }

    public Integer getNumeroRegistro() {
        return NumeroRegistro;
    }

    public void setNumeroRegistro(Integer numeroRegistro) {
        NumeroRegistro = numeroRegistro;
    }

    public String getDataNascimento() {
        return DataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        DataNascimento = dataNascimento;
    }
}

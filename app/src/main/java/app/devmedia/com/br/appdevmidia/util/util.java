package app.devmedia.com.br.appdevmidia.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import app.devmedia.com.br.appdevmidia.R;

/**
 * Created by andre.haas on 27/01/2017.
 */

public class util {

    public static void showMsgAlert ( final Activity activity, String titulo, String msg,
                                      boolean btn_cancelar){
        final AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        alertDialog.setIcon(R.drawable.common_google_signin_btn_icon_dark);
        alertDialog.setTitle(titulo);
        alertDialog.setMessage(msg);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //         util.showMsgToast(activity,"Nova conexao realizada...");
                alertDialog.dismiss();
            }
        });
        if (btn_cancelar) {
            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        }
        alertDialog.show();
    }
}
